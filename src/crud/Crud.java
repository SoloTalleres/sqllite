/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */

package crud;
import java.sql.*;

/**
 *
 * @author oscarmendezaguirre
 */
public class Crud {

    /**
     * @param args the command line arguments
     */

    private Connection connection;

    public Crud() {
        try {
            Class.forName("org.sqlite.JDBC");
            this.connection = DriverManager.getConnection("jdbc:sqlite:claseprogramacion1.db");
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
    }
    

    public void createTable() {
        try {
            Statement statement = connection.createStatement();
            statement.executeUpdate("CREATE TABLE IF NOT EXISTS users (id INTEGER PRIMARY KEY, name TEXT, age INTEGER)");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
        public void create(String name, int age) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO users(name, age) VALUES (?, ?)");
            preparedStatement.setString(1, name);
            preparedStatement.setInt(2, age);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
        
        
        public void readAll() {
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM users");

            while (resultSet.next()) {
                System.out.println(resultSet.getInt("id") + " " + resultSet.getString("name") + " " + resultSet.getInt("age"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
        
        
         public void update(int id, String name, int age) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("UPDATE users SET name=?, age=? WHERE id=?");
            preparedStatement.setString(1, name);
            preparedStatement.setInt(2, age);
            preparedStatement.setInt(3, id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void delete(int id) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM users WHERE id=?");
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
        
        
    
    
    public static void main(String[] args) {
        Crud crudSQLite = new Crud();
        crudSQLite.createTable();
        crudSQLite.create("Pepe", 25);
        crudSQLite.create("Maria", 30);
        crudSQLite.readAll();
        crudSQLite.update(1, "Juan", 26);
        crudSQLite.delete(2);
        crudSQLite.readAll();
    }
    
    
}
